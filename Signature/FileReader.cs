﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Signature
{
    public class FileReader
    {
        public event Action<BlockReadEventArgs> BlockRead;
        public event Action FileRead;

        public void ReadFileByBlock(string fileName, long blockSize)
        {
            FileInfo fileInfo = new FileInfo(fileName);
            long fileSize = fileInfo.Length;
            FileStream fs = new FileStream(fileName, FileMode.Open);    // reject "using" because of performance reason
            long blockCount = (long)Math.Ceiling((double)fileSize / blockSize);   
            long lastBlock = blockCount - 1;
            for (long i = 0; i < lastBlock; i++)
                ReadBlock(blockSize, fs, i);    // read block by block
            ReadBlock(fileSize % blockSize, fs, lastBlock);  // read last block, which can be smaller than other
            fs.Close();
            if (FileRead != null)
                FileRead();
        }

        private void ReadBlock(long blockSize, FileStream fs, long blockNumber)
        {
            byte[] data = new byte[blockSize];
            fs.Seek(blockNumber * blockSize, SeekOrigin.Begin);
            int maxFactor = int.MaxValue;   // Read() can't read more than int.Max bytes at a time
            if (blockSize <= maxFactor)
                fs.Read(data, 0, (int)blockSize);   // read in one pass
            else
            {
                long totalToRead = blockSize;
                int read = 0;
                while (totalToRead > 0) // read in several passes
                {
                    int toRead = totalToRead > maxFactor ? maxFactor : (int)totalToRead;
                    totalToRead = totalToRead - toRead;
                    fs.Read(data, read, toRead);
                    read += toRead;
                }
            }
            if (BlockRead != null)
                BlockRead(new BlockReadEventArgs(blockNumber, data));
        }
    }

    public class BlockReadEventArgs : EventArgs
    {
        public long BlockNumber { get; private set; }
        public byte[] Data { get; private set; }

        public BlockReadEventArgs (long blockNumber, byte[] data)
        {
            BlockNumber = blockNumber;
            Data = data;
        }
    }
}
