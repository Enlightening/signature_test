﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Security.Cryptography;

namespace Signature
{
    public class HashCalculatingThread
    {
        private Thread thread;
        private SHA256 sha256;
        private bool isWorking;
        private Dictionary<long, byte[]> queuedTasks;
        private object lockObject;
        private AutoResetEvent autoResetEvent;
        private SignatureSynchronizer synchronizer;

        public int TaskCount
        {
            get
            {
                return queuedTasks.Count;
            }
        }

        public HashCalculatingThread(SignatureSynchronizer synchronizer)
        {
            this.synchronizer = synchronizer;
            isWorking = true;   // to hold thread in working state
            lockObject = new object();
            queuedTasks = new Dictionary<long, byte[]>();   // block of file to calculate hash
            autoResetEvent = new AutoResetEvent(false);
            sha256 = SHA256.Create();
            thread = new Thread(Job);
            thread.Priority = ThreadPriority.BelowNormal;   // file read thread has higher priority 
            thread.Start();   
        }

        public void Feed(long blockNumber, byte[] data)
        {
            lock (lockObject)
            {
                queuedTasks.Add(blockNumber, data);
            }
            autoResetEvent.Set();   // wake up thread
        }

        public void Stop()
        {
            isWorking = false;
            autoResetEvent.Set();   // wake up thread to allow finish normally
        }

        private void Job()
        {
            while (isWorking || queuedTasks.Count > 0)  // work while isWorking and has task to perform
            {
                Dictionary<long, byte[]> tasks;
                lock (lockObject)
                {
                    if (queuedTasks.Count == 0)
                        continue;
                    tasks = new Dictionary<long, byte[]>(queuedTasks);  // copy task to free queuedTask for other thread
                    queuedTasks.Clear();
                }
                foreach (long blockNumber in tasks.Keys)
                {
                    byte[] hash = sha256.ComputeHash(tasks[blockNumber]);   // calculate hash block by block
                    string hashStr = GetStringFromByteArray(hash);
                    synchronizer.ShowSignatureSynchronized(blockNumber, hashStr);   // send hash to show
                }
                if (isWorking)
                    autoResetEvent.WaitOne();   // put to sleep if working
            }
        }

        private string GetStringFromByteArray(byte[] value)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in value)
                sb.AppendFormat("{0:X2}",b);  // format to hex
            return sb.ToString();
        }
    }
}
