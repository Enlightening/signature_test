﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Security.Cryptography;
using System.IO;

namespace Signature
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string fileName = args[0];
                if (!IsFileAccesible(fileName))
                    return;
                long blockSize;
                bool isBlockSizeNumber = long.TryParse(args[1], out blockSize);
                if (!isBlockSizeNumber && !IsBlockSizeAppropriate(blockSize))
                    return;
                FileReader fileReader = new FileReader();
                SignatureCalculator signatureCalc = new SignatureCalculator(blockSize);
                fileReader.BlockRead += (e) => signatureCalc.CalculateBlockHash(e.BlockNumber, e.Data);   // calculate hash of readed block
                fileReader.FileRead += () => signatureCalc.Stop();    // stop (all threads)
                fileReader.ReadFileByBlock(fileName, blockSize);    // start reading
            }
            catch (Exception e)
            {
                ShowException(e);
            }
        }

        private static bool IsFileAccesible(string fileName)
        {
            try
            {
                using (FileStream tempfs = new FileStream(fileName, FileMode.Open))
                { }
            }
            catch (Exception e)
            {
                ShowException(e);
                return false;
            }
            return true;
        }

        private static bool IsBlockSizeAppropriate(long blockSize)
        {
            if (blockSize <= 0)
            {
                Console.WriteLine("Unacceptable blocksize");
                return false;
            }
            try
            {
                byte[] a = new byte[blockSize];
            }
            catch (OutOfMemoryException e)
            {
                ShowException(e);
                return false;
            }
            return true;
        }

        private static void ShowException(Exception e)
        {
            Console.WriteLine(e);
            Console.ReadKey();
        }

    }
}
