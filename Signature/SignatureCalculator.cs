﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Diagnostics;

namespace Signature
{
    class SignatureCalculator
    {
        private List<HashCalculatingThread> workers;
        private double memoryToUse;
        private long memoryPerTask;

        public SignatureCalculator(long blockSize)
        {
            memoryToUse = GetAvailableMemoryInBytes() /4;   // get memory available for app. It includes not only ram, so we divide by two. Another division - an empiric value for performance
            memoryPerTask = sizeof(long) + blockSize;   // memory used per task ( byte[] size as value + key )
            SignatureSynchronizer synchronizer = new SignatureSynchronizer();   // object for synchronizing result
            int workersCount = System.Environment.ProcessorCount;
            workers = new List<HashCalculatingThread>(workersCount);
            for (int i = 0; i < workersCount; i++)
                workers.Add(new HashCalculatingThread(synchronizer));
        }

        private static float GetAvailableMemoryInBytes()
        {
            PerformanceCounter memoryCounter = new PerformanceCounter("Memory", "Available MBytes");
            float totalMemoryInMegabyte = memoryCounter.NextValue();
            float available = (totalMemoryInMegabyte * 1024 * 1024) - Process.GetCurrentProcess().PrivateMemorySize64;
            return available;
        }

        public void Stop()
        {
            workers.ForEach(w=>w.Stop());   // stop all threads
        }

        public void CalculateBlockHash(long blockNumber, byte[] data)
        {
            while (true)
            {
                long memoryUsed = workers.Sum(w => w.TaskCount) * memoryPerTask;    // approximate memory used for tasks
                if (memoryUsed < memoryToUse)   // add task to queue if have enough memory
                {
                    workers.OrderBy(w => w.TaskCount).First().Feed(blockNumber, data);  // actual calculating will be performed in a thread with smaller job queue
                    break;  // exit loop if task added
                }
                else
                    Thread.Sleep(32);   // wait workers to complete tasks, allowing GC to free memory (blocking disk reading thread)
            }
        }


    }
}
