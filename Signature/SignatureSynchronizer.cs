﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Signature
{
    public class SignatureSynchronizer
    {
        private long blockHashToShow;
        private object lockObject;
        private SortedDictionary<long, string> signatures;

        public SignatureSynchronizer()
        {
            blockHashToShow = 0;    // block number needed to show in console
            lockObject = new object();  // for synchronizing acces to signatures
            signatures = new SortedDictionary<long, string>();  // hashes waiting to be shown
        }

        public void ShowSignatureSynchronized(long blockNumber, string hash)
        {
            lock (lockObject)
            {
                signatures.Add(blockNumber, hash);  // to maintain in sorted order
                long firstKey = signatures.First().Key;
                while (firstKey == blockHashToShow) // showhash in block order, or break and wait until next calculated hash
                {
                    Console.WriteLine("{0}. {1}", firstKey, signatures[firstKey]);
                    signatures.Remove(firstKey);    // remove hash to not to show again
                    blockHashToShow++;  // need next block
                    if (signatures.Count > 0)
                        firstKey = signatures.First().Key;  // next block in order
                    else
                        break;
                }
            }
        }
    }
}
